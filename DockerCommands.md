# Comandos utiles en docker

---

Pequeña chuleta con comandos docker para trabajar con la imagenes y contenedores de docker.

Comandos para gestionar

* **docker pull** *NOMBREIMAGEN . *Este comando sirve para descargar una imagen* *
* **docker images  **muestra imágenes que tenemos descargadas
* **docker ps -a**
  ```
  muestra que contenedores estan funcionando
  ```
* **docker rmi  *IMAGE_ID***
* **docker rmi** docker rmi -f  $(docker images -a -q)
* **docker info**  Muestra informacion de las imagenes, tamaño , fecha creacion  nombre ,…
* **docker search NAME**   para buscar in docker
* **docker inspect <friendly-name|container-id>** . para saber acerca del contenedor
* **docker logs <friendly-name|container-id>**

Comandos para el contenedor

* **docker start**  --> Inicia el contenedor.
* **docker stop**  --> Detiene el contenedor.
* **docker kill**   --> Elimina uno o mas contenedores en ejecución.
* **docker rm** --> Remueve el contenedor.

Docker composer

* **docker-compose up -d**  // Levantar la maquina con un fichero docker-compose  hay que entrar en la carpeta donde este el fichero
* **docker-compose down** // Detiene y elimina el contenedor.
* docker-compose start    // Iniciar el ambiente de desarrollo
  docker-compose stop     # Detener el ambiente de desarrollo
